# Practice workflow
1. Make sure you are in main
2. git fetch/pull origin main
3. Create a new branch depending on the [_feature_](#tasks) you are going to work on
4. Add/Commit
5. Push the branch to Gitlab -> git push origin BranchName

# Tasks
You can do whatever you want as long as you modify something and follow the [workflow above](#practice-workflow). If you don't know what to do simply modify this markdown file or the README.md file.

## IMPORTANT
Whatever you do let the group know so we don't have overlapping modification to start with.

## Suggestions:
- Improve segmentation by modifying only the function ThresholdBinary or adding a new function
- Measure more parameters in the function MeasureParticles
- Try to apply the whole analysis to an image of your choice
- Constrain the Analyze Particles to some values of _circularity_
- Modify the file 02_DescriptiveStatistics.py to produce more percentiles in the descriptive statistics table
- Add a title in final figure modifying 03_PlotMeasurements.py
- Change legend title in figure modifying 03_PlotMeasurements.py
- Modify the shape of figure from 2x2 to 1x4 in 03_PlotMeasurements.py

# FOR THE END: Practice conflict
Now that we are familiar with the normal workflow, let's practice some merge conflicts. 

Agree with a groupmate something to _work on_. Each of you should create a separate independent branch. 

Make sure to both modify **at least one line in the same position** (for example you both modify the name of Function OpenImage in line 16 in the 01_Segmentation_AnalyzeParticles.ijm file).

After you both created new branches and made the modifications, we will see together what happens when we try to merge them.